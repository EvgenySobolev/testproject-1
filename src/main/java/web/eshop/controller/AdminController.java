package web.eshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import web.eshop.model.Product;
import web.eshop.model.Role;
import web.eshop.model.User;
import web.eshop.service.ProductService;
import web.eshop.service.UserService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class AdminController {
    @Autowired
    private UserService service;
    @Autowired
    private ProductService productService;

    @GetMapping("/admin")
    public String showAdminPage(Model model) {
        model.addAttribute("user", new User());

        return "/admin/index";
    }

    @GetMapping("/admin/users")
    public String listUsers(Model model) {
        List<User> listUsers = service.listAll();
        listUsers.sort((o1, o2) -> Math.toIntExact(o1.getId() - o2.getId()));
        model.addAttribute("listUsers", listUsers);

        return "admin/users";
    }

    @GetMapping("/admin/users/edit/{id}")
    public String editUser(@PathVariable("id") Long id, Model model) {
        User user = service.get(id);
        List<Role> listRoles = service.listRoles();
        model.addAttribute("user", user);
        model.addAttribute("listRoles", listRoles);
        return "admin/user_form";
    }

    @PostMapping("/admin/users/save")
    public String saveUser(User user) {
        service.save(user);

        return "redirect:/admin/users";
    }


    @GetMapping("/admin/catalog")
    public String viewCatalogPage(Model model) {
        model.addAttribute("product", new Product());
        List<Product> listProducts = productService.listAll();
        listProducts.sort((o1, o2) -> Math.toIntExact(o1.getId() - o2.getId()));
        model.addAttribute("listProducts", listProducts);

        return "admin/catalog";
    }

    @GetMapping("/admin/catalog/edit/{id}")
    public String editProduct(@PathVariable("id") Long id, Model model) {
        Product product  = productService.get(id);
        model.addAttribute("product", product);
        return "admin/catalog_detail";
    }
    @PostMapping("/admin/product/save")
    public String saveProduct(Product product) {
        productService.save(product);

        return "redirect:/admin/catalog";
    }

    @PostMapping("/admin/product/delete/{id}")
    public String deleteProduct(@PathVariable("id") Long id, Model model) {
        Product product = productService.get(id);
        productService.delete(product);

        return "redirect:/admin/catalog";
    }
    @GetMapping("admin/catalog/add")
    public String addgProduct(Model model){
        model.addAttribute("product",new Product());
        return  "admin/catalog_add";
    }

    @PostMapping("admin/product/add")
    public String addProduct(Product product){
        productService.add(product);
        return  "redirect:/admin/catalog";
    }

    @PostMapping("/admin/users/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id, Model model) {
        User user = service.get(id);
        service.delete(user);

        return "redirect:/admin/users";
    }

}
