package web.eshop.controller;

import web.eshop.dto.SignUpForm;
import web.eshop.model.User;
import web.eshop.service.SignUpService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class SignUpController {

    private final SignUpService signUpService;

    	@GetMapping("/register")
	public String showRegistrationForm(Model model) {
		model.addAttribute("user", new User());

		return "signup";
	}

//    @PostMapping("/register")
//    public String signUp(@Valid SignUpForm form, BindingResult result, Model model) {
//        if (result.hasErrors()) {
//            model.addAttribute(form);
//            return "signup";
//        }
//        signUpService.signUp(form);
//        return "redirect:/signIn";
//    }
}
