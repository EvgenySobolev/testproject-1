package web.eshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import web.eshop.model.Role;
import web.eshop.model.User;
import web.eshop.service.UserService;

@Controller
public class AppController {

	@Autowired
	private UserService service;


	@GetMapping("")
	public String viewHomePage() {
		return "index";
	}

	@GetMapping("/error")
	public String viewErrorPage() {
		return "error";
	}
	
//	@GetMapping("/register")
//	public String showRegistrationForm(Model model) {
//		model.addAttribute("user", new User());
//
//		return "signup";
//	}

	@GetMapping("/login")
	public String showLoginForm(Model model) {
		model.addAttribute("user", new User());

		return "login";
	}
	
	@PostMapping("/process_register")
	public String processRegister(User user) {
		service.registerDefaultUser(user);
		
		return "register_success";
	}
}
