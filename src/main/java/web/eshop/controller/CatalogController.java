package web.eshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import web.eshop.model.Product;
import web.eshop.service.ProductService;

import java.util.List;

@Controller
public class CatalogController {

    @Autowired
    private ProductService productService;

    @GetMapping("/catalog")
    public String viewCatalogPage(Model model) {
        model.addAttribute("product", new Product());

        List<Product> listProducts = productService.listAll();
        model.addAttribute("listProducts", listProducts);

        return "catalog";
    }

}
