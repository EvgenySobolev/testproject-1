package web.eshop.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import web.eshop.dao.CustomUserDetails;
import web.eshop.model.CartLine;
import web.eshop.model.Product;
import web.eshop.model.User;
import web.eshop.model.Cart;
import web.eshop.service.CartService;
import web.eshop.service.ProductService;
import web.eshop.service.UserService;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;


@Controller
@RequiredArgsConstructor
public class CartController {

    @Autowired
    CartService cartService;

    @Autowired
    ProductService productService;

    @Autowired
    UserService userService;


    @GetMapping("/cart/add/{id}")
    public String addToCart(@PathVariable("id") Long id, Model model, Authentication authentication, Principal principal) {
        CustomUserDetails details = (CustomUserDetails) authentication.getPrincipal();
        User user = userService.get(details.getUserId());
        Product product = productService.get(id);
        Cart cart = new Cart();
        cart.setUser(user);
        Long cartId = cartService.add(cart);
        List<CartLine> productList = new ArrayList<CartLine>();
        CartLine cartLine = new  CartLine();
        cartLine.setCart_id(cartId);
        cartLine.setPrice(product.getPrice());
        cartLine.setQuantity(1);
        cartLine.setTotal(product.getPrice());
        cartLine.setProduct_id(product.getId());
                productList.add(cartLine);
//        cart.setProductList(productList);
        cartService.save(cart);
//        cart.setProductList(productList);

        return "redirect:/catalog/";
    }





}
