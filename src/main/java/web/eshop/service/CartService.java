package web.eshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.eshop.model.Cart;
import web.eshop.repository.CartRepository;

import java.util.List;
@Service
public class CartService {
    @Autowired
    CartRepository cartRepository;

    public List<Cart> listAll() {
        return cartRepository.findAll();
    }

    public Cart get(Long id) {
        return cartRepository.findById(id).get();
    }

    public void save(Cart cart) {
        cartRepository.save(cart);
    }

    public void delete(Cart cart){
        cartRepository.delete(cart);
    }
    public Long add(Cart cart){
        cartRepository.save(cart);
        cartRepository.flush();
        return cart.getId();
    }

}
