package web.eshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import web.eshop.model.Product;
import web.eshop.repository.ProductRepository;


import java.util.List;

@Service
public class ProductService {
	@Autowired
	private ProductRepository productRepository;
	public List<Product> listAll() {
		return productRepository.findAll();
	}

	public Product get(Long id) {
		return productRepository.findById(id).get();
	}

	public void save(Product product) {
		productRepository.save(product);
	}

	public void delete(Product product){
		productRepository.delete(product);
	}
	public void add(Product product){
		productRepository.save(product);
	}

}
