package web.eshop.service;

import web.eshop.dto.SignUpForm;
import web.eshop.model.User;
import web.eshop.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SignUpServiceImpl implements SignUpService {

    private final UserRepository accountsRepository;

    private final PasswordEncoder passwordEncoder;
    @Override
    public void signUp(SignUpForm form) {
        User account = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
//                .role(User.Role.USER)
//                .state(User.State.NOT_CONFIRMED)
                .password(passwordEncoder.encode(form.getPassword()))
                .build();

        accountsRepository.save(account);
    }
}
