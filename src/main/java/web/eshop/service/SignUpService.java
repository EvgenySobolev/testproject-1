package web.eshop.service;

import web.eshop.dto.SignUpForm;

public interface SignUpService {

    void signUp(SignUpForm form);
}

