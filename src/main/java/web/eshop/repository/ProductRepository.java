package web.eshop.repository;

import web.eshop.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductRepository extends JpaRepository<Product, Long> {

//    List<Account> findAllByPassword(String password);
//
//    Account findFirstByPassword(String password);
//
//    @Query("select account from Account account join fetch account.cars car where car.color = :color")
//    List<Account> findAllByCarsColor(@Param("color") String color);
}